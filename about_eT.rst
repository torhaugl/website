.. _about-eT:


The eT program
==============

eT is an electronic structure program written in the Fortran 2008 standard
with focus on 
coupled cluster
and multilevel methods.
The program is
`open source <https://gitlab.com/eT-program/eT>`_ under the 
`GPLv3 <https://www.gnu.org/licenses/gpl-3.0.html>`_ license, 
easy to use, rigorously tested,
and optimized for performance.

We are always looking for collaborators. 
If you wish to contribute, 
please visit the 
`for developers <https://gitlab.com/eT-program/eT/-/wikis/home#writing-code-in-et>`_
page on the wiki.

The eT collaboration
--------------------

eT is primarily developed by research groups at the Norwegian University of Science and Technology (`NTNU <https://www.ntnu.edu/chemistry/research/theoretical/quantum#/view/about>`_) in Trondheim, Norway,
Scuola Normale Superiore (`SNS <https://www.sns.it/it/koch-henrik>`_) in Pisa, Italy, and the Technical University of Denmark (`DTU <https://www.kemi.dtu.dk/english/Research/PhysicalChemistry/Femtokemi/SoniaCoriani>`_) in Copenhagen, Denmark.

Coordinator
^^^^^^^^^^^
`Henrik Koch <https://www.sns.it/it/koch-henrik>`_ (SNS)


Lead developers 
^^^^^^^^^^^^^^^
`S\. D. Folkestad <https://www.ntnu.no/ansatte/sarai.d.folkestad>`_ (NTNU), 
`E\. F\. Kjønstad <https://www.ntnu.no/ansatte/eirik.kjonstad>`_ (NTNU), 
`R\. H\. Myhre <https://www.ntnu.no/ansatte/rolf.h.myhre>`_ (NTNU),
`A\. C\. Paul <https://www.ntnu.edu/employees/alexander.c.paul>`_ (NTNU)


Developers
^^^^^^^^^^
`J\. H. Andersen <https://www.dtu.dk/service/telefonbog/person?id=97881&cpid=279455&tab=3&qt=dtuprojectquery>`_ (DTU), 
`A\. Balbi <https://www.sns.it/en/balbi-alice>`_ (SNS), 
`S. Coriani <https://www.kemi.dtu.dk/english/research/physicalchemistry/femtokemi/soniacoriani/profile>`_ (DTU), 
`T\. Giovannini <https://www.ntnu.no/ansatte/tommaso.giovannini>`_ (NTNU),
`L\. Goletto <https://www.ntnu.edu/employees/linda.goletto>`_ (NTNU), 
`T\. S\. Haugland <https://www.ntnu.edu/employees/tor.s.haugland>`_ (NTNU), 
`A\. Hutcheson <https://www.ntnu.edu/employees/anders.hutcheson>`_ (NTNU),
`I-M\. Høyvik <https://www.ntnu.edu/employees/ida-marie.hoyvik>`_ (NTNU),
`T. Moitra <https://www.dtu.dk/service/telefonbog/person?id=136590&cpid=247062&tab=2&qt=dtupublicationquery>`_ (DTU), 
`S. Roet <https://www.ntnu.edu/employees/sander.roet>`_ (NTNU), 
`M. Scavino <https://www.sns.it/en/scavino-marco>`_ (SNS), 
`A\. Skeidsvoll <https://www.ntnu.edu/employees/andreas.skeidsvoll>`_ (NTNU),
Å\. H\. Tveten (NTNU)


Funding and support
-------------------

 - Norwegian University of Science and Technology (NTNU)
 - Scuola Normale Superiore (SNS)
 - Technical University of Denmark (DTU)
 - UNINETT Sigma2 -- the National Infrastructure for High Performance Computing and Data Storage in Norway
 - Marie Skłodowska-Curie European Training Network `COSINE -- COmputational Spectroscopy In Natural sciences and Engineering`
 - Research Council of Norway -- FRINATEK, projects 263110 and 275506.
 - Independent Research Fund Denmark -- Natural sciences, research project 2 no. 7014-00258B

How to cite eT
--------------
S. D. Folkestad, E. F. Kjønstad, R. H. Myhre, J. H. Andersen, A. Balbi, S. Coriani, T. Giovannini, L. Goletto, T. S. Haugland, A. Hutcheson, I-M. Høyvik, T. Moitra, A. C. Paul, M. Scavino, A. S. Skeidsvoll, Å. H. Tveten, and H. Koch, 
*"eT 1.0: An open source electronic structure program with emphasis on coupled cluster and multilevel methods",* 
J. Chem. Phys. 152, 184103 (2020), `https://doi.org/10.1063/5.0004713 <https://aip.scitation.org/doi/full/10.1063/5.0004713>`_ 

Contact
-------

Contact us at support@etprogram.org.


