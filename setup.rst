.. _setup:

How to use the setup script
============================

This page describes the ``setup`` script and its options.

What is the setup script
------------------------

The setup script is a python script located in the eT base directory.
It requires at least Python 3.5 and uses the argparse module to parse command line arguments.
The arguments are then passed on to `CMake <https://cmake.org/>`_ 
which takes care of the configuration of eT.
All the options sent to CMake via the setup script can be passed directly to CMake,
but this is usually a lot more painful.

If you think the default values are good for you, 
you can run the script with:

.. code-block:: none

   ./setup

This will generate a directory called build that should contain your Makefile.
You can then go to this directory and compile using ``make``.

.. code-block:: none

   make -j4

The ``-j4`` option enables parallel compilation with four threads and can really speed things up.

For a list of arguments and a short description of each, as well as defaults, use:

.. code-block:: none

   ./setup --help


List of setup options
---------------------

This is a list of the arguments accepted by ``setup``. 
Most of them come in two versions, 
a long and a short. 
The long form starts with two dashes, 
while the shorthand begins with one dash.
For example, 
``--libint-lib`` and ``-ll`` are equivalent.

Positional argument
^^^^^^^^^^^^^^^^^^^

.. container:: sphinx-custom

   ``build_dir`` 

   This is the only positional argument, 
   so it can be placed anywhere, 
   except after an option that expects a string argument
   like ``-extra-F-flags``. 
   The argument shall be a string with the directory you want to compile eT in.
   Note that it is not necessary to put you build directory in the ``eT`` directory.
   For example, ``./setup ~/progs/eT``, 
   will create the directory ``~/progs/eT``, 
   assuming you has access, 
   and place the build files there.
   ``Default: build``

Optional arguments
^^^^^^^^^^^^^^^^^^

These are arguments to specify how you want to compile eT.

.. container:: sphinx-custom

   ``-h, --help`` 

   As mentioned above, 
   this option will not run the script,
   but simply list the available options for ``setup``.
   This is a built-in argparse option.

.. container:: sphinx-custom

   ``-clean, --clean-dir`` 

   This option will make ``setup`` delete your 
   build directory if it exists before calling CMake. 
   Very handy when you want to force a clean compilation.
   ``Default: False``

.. container:: sphinx-custom

   ``-cleanC, --clean-CMake`` 
   
   Similar to the above, 
   but will only delete ``CMakeCache.txt``,
   if it exists, 
   forcing a recompilation.
   `Default: False`

.. container:: sphinx-custom

   ``-ll, --libint-lib`` 
   
   Specifies a path where CMake should look for Libint
   and will override the ``LIBINT2_ROOT`` environment variable.

.. container:: sphinx-custom

   ``-FC, Fortran-compiler`` 

   Exactly what it says on the label. 
   Specifies which Fortran compiler CMake should look for.

.. container:: sphinx-custom

   ``-CXX, CXX-compiler`` 
   
   Specifies which C++ compiler CMake should look for.

.. container:: sphinx-custom

   ``-CC, C-compiler``

   Specifies which C compiler CMake should look for.

.. container:: sphinx-custom

   ``--int64`` 
   
   Enables 64-bit integers. 
   Note that MKL or another 64-bit implementation of BLAS/LAPACK
   is required for this option. 
   In particular, OpenBLAS and SYSTEM_NATIVE are only available for 32-bit integers.
   If you are using one of these, 
   use the ``--int32`` option.

   ``Excludes: --int32``,
   ``Default: True``

.. container:: sphinx-custom

   ``--int32`` 

   Enables 32-bit integers. 

   ``Excludes: --int64``,
   ``Default: False``

.. container:: sphinx-custom

   ``--omp``

   Enable OpenMP multithreading.

   ``Excludes: --no-omp``,
   ``Default: True``

.. container:: sphinx-custom

   ``--no-omp`` 

   Enable OpenMP multithreading.

   ``Excludes: --omp``,
   ``Default: True``

.. container:: sphinx-custom

   ``--blas-type`` 

   Specifies the search order for the math BLAS/LAPACK library.
   CMake will search based on the ``MATH_ROOT`` environment varaible.

   ``Default with --int64: MKL ACML SYSTEM_NATIVE ATLAS``,

   ``Default with --int32: MKL SYSTEM_NATIVE OPENBLAS ATLAS ESSL ACML``

.. container:: sphinx-custom

   ``--pcm`` 

   Enables compiling eT with linking to PCMSolver.
   PCMSolver will be searched for based on the environment variable ``PCMSolver_ROOT``.

   ``Default: False``

Extra flags
^^^^^^^^^^^

These arguments are passed more or less directly to CMake or the compiler.
Because flags are also specified beginning with `-`,
it is necessary to pass these arguments explicitly as strings starting with a white space.
For example,
if you want to compile with CMake ninja, 
you would use:

.. code-block:: none

   ./setup --extra-cmake-flags " -GNinja"

Several flags can be specified in the string.

.. container:: sphinx-custom

   ``-cmake-flags, --extra-cmake-flags`` 

   Extra flags sent to CMake.

.. container:: sphinx-custom

   ``-F-flags, --extra-F-flags`` 

   Extra compile flags set for the Fortran compiler.

.. container:: sphinx-custom

   ``-CXX-flags, --extra-CXX-flags`` 

   Extra compile flags set for the C++ compiler.

.. container:: sphinx-custom

   ``-linker-flags, --extra-linker-flags`` 

   Extra flags set for the compiler during linking.


Debug arguments
^^^^^^^^^^^^^^^

These are arguments mostly meant for developers,
and should not be used for production code.

.. container:: sphinx-custom

   ``-rcheck, --runtime-checks`` 

   Enables runtime checks, 
   making the compiler put in checks for various memory errors at runtime.
                               
.. container:: sphinx-custom

   ``-fbatch, --forced-batching`` 

   Forces eT to always batch with random batch sizes in routines 
   implemented with memory batching. 
   Useful for catching errors.

