
do
--

The ``do`` section is where the type of calculation is specified. It will determine the :math:`e^T` engine used in the calculation.

.. note:: 
   
   Only one of the keywords below has to be specified.
   For example for the calculation of excited states only the keyword ``excited state`` is required 
   even though the ground state equations have to be solved as well to obtain excited states.

Keywords
^^^^^^^^

.. container:: sphinx-custom
   
   ``cholesky eri``

   Keyword to run a Cholesky decomposition of the two-electron integrals. Note that this is done automatically for any coupled cluster calculation, the keyword should only be given if only Cholesky decomposition is to be performed.

.. container:: sphinx-custom

   ``ground state``

   Keyword to run a ground state calculation at the level of theory given in the ``method`` section.
   Enables the ground state or reference engine.

.. container:: sphinx-custom

   ``ground state geoopt``

   Keyword to run a Hartree-Fock ground state geometry optimization. Enables the ground state geometry optimization engine.

.. container:: sphinx-custom

   ``mean value``

   Keyword to calculate *coupled cluster* expectation values. Enables the mean value engine, which determines the coupled cluster ground state amplitudes and multipliers, and calculates the requested expectation value. Which mean value(s) to calculate are specified in the ``cc mean value`` section.

   .. note::

      For Hartree-Fock calculations, one must write ``ground state`` in ``do`` and specify the mean value(s) to calculate in the ``hf mean value`` section.


.. container:: sphinx-custom

   ``excited state``

   Keyword to run a coupled cluster excited state calculation. Enables the excited state engine, which calculates the ground and excited state amplitudes.

   .. note::

      The ``cc es solver`` section is required for excited state calculations.

.. container:: sphinx-custom

   ``response``

   Keyword to enable the coupled cluster response engine. Implemented features are EOM transition moments and polarizabilities for CCS, CC2, CCSD and CC3, and LR transition moments and polarizabilities for CCS.
   The response engine drives the calculation of ground state amplitudes and multipliers, excited state vectors (left and right eigenvectors of the Jacobian matrix) and the requested property.

   .. note::

      The ``cc response`` section is required for coupled cluster response calculations.

.. container:: sphinx-custom

   ``restart``

   Global restart keyword to activate restart where possible. 

   .. note::
 
      eT will first check if restart is possible and use the default start guess if not.

.. container:: sphinx-custom

   ``time dependent state``

   Keyword to run coupled cluster time propagation. Enables the time-dependent engine.
   
   .. note::


      The ``cc td`` section is required for coupled cluster time propagation.

Example
^^^^^^^
To calculate the CCSD ground and four excited states, specify

.. code-block:: none

   do
      excited state
   end do

together with

.. code-block:: none

   method
      hf
      ccsd
   end method

and 

.. code-block:: none 

   solver cc es
      singlet states: 4
   end solver cc es


