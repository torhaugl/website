memory
------

Keywords to specify the available memory is given in the ``memory`` section.

Keywords
^^^^^^^^

.. container:: sphinx-custom
	
	``available: [integer]``

	Default: 8

	Specifies the available memory, default units are gigabytes. Optional.

.. container:: sphinx-custom

	``unit: [string]``

	Default: GB

	Specifies the units for the specified available memory. Optional.

	Valid keyword values are:

	- B
	- MB
	- GB
