solver cc multipliers
---------------------

Keywords related to solving the coupled cluster multiplier equation (left coupled cluster ground state) go into this section.

Keywords
^^^^^^^^

.. container:: sphinx-custom

   ``algorithm: [string]``

   Default: ``diis`` for ``ccs``, ``lowmem-cc2``, ``cc2``, and ``cc3``; ``davidson`` for other coupled cluster models. 

   Solver to use for converging the multiplier equation. Not supported for MLCC2.

   Valid keyword values are:

   - ``davidson`` Use Davidson algorithm with residuals preconditioned with orbital differences approximation of the Jacobian. Cannot currently be used for lowmem-CC2, CC2, and CC3.
   - ``diis`` Use DIIS algorithm with update estimates obtained from the orbital differences approximation of the Jacobian. Must be used for lowmem-CC2, CC2, and CC3.

.. container:: sphinx-custom

   ``threshold: [real]``

   Default: :math:`10^{-5}` (or 1.0d-5)

   Threshold of the :math:`L^{2}`-norm of the residual vector of the multiplier equation. Optional.

.. container:: sphinx-custom

   ``max iterations: [integer]``

   Default: 100

   The maximum number of iterations. The solver stops if the number of iterations exceeds this number. 
   Optional.

.. container:: sphinx-custom

   ``diis dimension: [integer]``

   Default: 20

   Number of previous DIIS records to keep.  Optional.

   .. note::
      Only relevant for ``algorithm: diis``.

.. container:: sphinx-custom

   ``restart``

   Default: false

   If specified, the solver will attempt to restart from a previous calculation. Optional.

.. container:: sphinx-custom

   ``storage: [string]``

   Default: ``disk``

   Selects storage of solver subspace records. Optional. 

   Valid keyword values are:

   - ``memory`` Stores records in memory.
   - ``disk`` Stores records on file.

.. container:: sphinx-custom

   ``crop``

   Default: false

   If specified, the CROP version of DIIS will be enabled. Optional.

   .. note::
      Only relevant for ``algorithm: diis``.

.. container:: sphinx-custom

   ``max reduced dimension: [integer]``

   Default: 100

   The maximal dimension of the reduced space of the Davidson procedure. Optional.

   .. note::
      Only relevant for ``algorithm: davidson``.

