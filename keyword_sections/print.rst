
print
-----

In this section you can specify settings related to the main output files.

Keywords
^^^^^^^^

.. container:: sphinx-custom
   
   ``output print level: [string]``

   Specifies the print level for the main output file. Default is ``normal``. Optional.
   
   Valid keyword values are:

   - ``minimal`` Only banners, final results like total energies or excitation energies, solver settings, and other essential information.

   - ``normal`` In addition to minimal, print iteration information and details like orbital energies, amplitude analysis, and other non-essential information.

   - ``verbose`` In addition to normal, print all relevant information for users. This can make the output difficult to read and navigate.

   - ``debug`` In addition to verbose, prints information mostly relevant for debugging code that behaves unexpectedly.

.. container:: sphinx-custom

   ``timing print level: [string]``

   Specifies the print level for the timing file. Default is ``normal``. Optional.
   
   Valid keyword values are:

   - ``minimal`` Total solver timings, total program time, and other essential timings.

   - ``normal`` In addition to minimal, iteration times and details like time to calculate omega, the Fock matrix, and other expensive terms.

   - ``verbose`` In addition to normal, times for subtasks, such as micro-iteration times as well as individual contributions to the omega vector.

   - ``debug`` In addition to verbose, prints timings mostly relevant for debugging code that behaves unexpectedly.
