geometry
--------

The geometry must be given as xyz coordinates either with Angstrom or Bohr units. 
The default units are Angstrom. 
The basis set is also given in the geometry section.

Minimal example for the geometry section

.. code-block:: none
	
	geometry
	   basis: aug-cc-pVDZ
	   H          0.86681        0.60144        5.00000
 	   H         -0.86681        0.60144        5.00000
 	   O          0.00000       -0.07579        5.00000
	end geometry

If other units than ``Angstrom`` are desired,
this must be specified at the top of the geometry section with the ``units`` keyword.
Possible values are ``Angstrom`` and ``Bohr``.

.. code-block:: none
	
	geometry
	   units: Bohr
	   basis: aug-cc-pVDZ
	   H          1.63803   1.13655   9.44863
 	   H         -1.63803   1.13655   9.44863
 	   O          0.00000  -0.14322   9.44863
	end geometry

Different basis sets may be placed on the atoms using the ``basis`` keyword. 
An atom is given the last basis set specified above it, 
e.g. in the following example the oxygen has the d-aug-cc-pVDZ basis 
and the hydrogens have the aug-cc-pVDZ basis.
See :ref:`here <basis-sets>` for a list of included basis sets.

.. code-block:: none
	
	geometry
	   basis: aug-cc-pVDZ
	   H          0.86681        0.60144        5.00000
 	   H         -0.86681        0.60144        5.00000
	   basis: d-aug-cc-pVDZ
 	   O          0.00000       -0.07579        5.00000
	end geometry

Basis functions can be placed without the corresponding atom by using the keyword
``ghost``. Every atom specified after the keyword will have zero charge, which corresponds
to only placing the basis functions. An H2 calculation run with the 
same basis functions as H2O can be done using this geometry:

.. code-block:: none

	geometry
	   basis: aug-cc-pVDZ
	   H          0.86681        0.60144        5.00000
 	   H         -0.86681        0.60144        5.00000
	   ghost
 	   O          0.00000       -0.07579        5.00000
	end geometry
        

In case of QM/MM calculations, 
the QM and MM portions have to be separated by a line containing ``--``. 
The parameters for QM/MM calculations are placed after the XYZ coordinates. 
In case of electrostatic QM/MM embedding (see :ref:`molecular mechanics section<mm-section>`), 
the charge of each atom needs to be provided:

.. code-block:: none

    geometry
       basis: cc-pVDZ
        O      0.87273600      0.00000000     -1.24675400
        H      0.28827300      0.00000000     -2.01085300
        H      0.28827300      0.00000000     -0.48265500
        --
        O [IMol=   1]     -0.77880300      0.00000000      1.13268300      [q=-0.834]
        H [IMol=   1]     -0.66668200      0.76409900      1.70629100      [q=+0.417]
        H [IMol=   1]     -0.66668200     -0.76409900      1.70629000      [q=+0.417]
    end geometry

In case of polarizable QM/FQ (see :ref:`molecular mechanics section<mm-section>`), 
the electronegativities (chi) and chemical hardnesses (eta) are needed for each atom:

.. code-block:: none

    geometry
       basis: cc-pVDZ
        O      0.87273600      0.00000000     -1.24675400
        H      0.28827300      0.00000000     -2.01085300
        H      0.28827300      0.00000000     -0.48265500
        --
        O [IMol=   1]     -0.77880300      0.00000000      1.13268300     [chi=0.11685879436,eta=0.58485173233]
        H [IMol=   1]     -0.66668200      0.76409900      1.70629100     [chi=0.00000000000,eta=0.62501048888]
        H [IMol=   1]     -0.66668200     -0.76409900      1.70629000     [chi=0.00000000000,eta=0.62501048888]
    end geometry

