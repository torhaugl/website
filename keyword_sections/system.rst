system
------

In the system section of the input the name, charge, and multiplicity of the system is given. Furthermore, the use of cartesian Gaussians may be specified in this section.


Required keywords
^^^^^^^^^^^^^^^^^

.. container:: sphinx-custom
	
	``name: [string]``

	Default: none

	Specifies the name of the calculation, which is printed in the output file.


Optional keywords
^^^^^^^^^^^^^^^^^

.. container:: sphinx-custom

	``charge: [integer]``

	Default: 
	:math:`0`

	Specifies the charge of the system.

.. container:: sphinx-custom

	``multiplicity: [integer]``

	Default: 
	:math:`1`

	Specifies the spin multiplicity of the system.

.. container:: sphinx-custom
	
	``cartesian gaussians``

	Enforce cartesian Gaussians basis functions for all atoms. Default for Pople basis sets.
   
.. container:: sphinx-custom

   ``pure gaussians``

   Enforce spherical Gaussian basis functions for all atoms. Default for all basis sets except Pople basis sets.


Examples
^^^^^^^^

A minimal example of the *system* section, includes only the name of the calculation. This can be any string, e.g., 

.. code-block:: none
   
   system
      name: water
   end system

The charge and multiplicity is given in the example below. Additionally, cartesian Gaussians are enabled.

.. code-block:: none
   
   system
      name: water
      charge: 0
      multiplicity: 1
      cartesian gaussians
   end system
