cc response
-----------
Keywords specific to response calculations are given in the ``cc response`` section.
This currently involves EOM transition moments for CCS, CC2, CCSD, and CC3,
EOM polarizabilities for CCS, and CCSD,
and linear response transition moments and polarizabilities at the CCS level of theory.

.. note::
   This section is required if the keyword ``response`` is given in the ``do`` section.

Keywords
^^^^^^^^
.. container:: sphinx-custom

   ``polarizabilities``

   Enables the calculation of polarizabilities. Either this or the ``transition moments`` keyword must be specified.

.. container:: sphinx-custom

   ``transition moments``

   Enables the calculation of transition moments. Either this or the ``polarizabilities`` keyword must be specified.

.. container:: sphinx-custom

   ``eom``
   
   Properties will be calculated within the equation of motion formalism.

   Either this or the ``lr`` keyword must be specified. 
   
   Available for CCS, CC2, CCSD, and CC3.

.. container:: sphinx-custom

   ``lr``
   
   Properties will be calculated within the linear response formalism.

   Either this or the ``eom`` keyword must be specified. 
   
   Available for CCS.

.. container:: sphinx-custom

   ``dipole length``

   Required keyword. Currently the only operator available for response calculations in :math:`e^T`.
   
.. container:: sphinx-custom

   ``frequencies: {[real], [real], ...}``

   Frequencies for which the polarizability shall be computed. Required for polarizabilities.
   
