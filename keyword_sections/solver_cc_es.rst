solver cc es
------------

Keywords related to solving the excited state coupled cluster equations go into the ``solver cc es`` section. 
Required for calculations of excited states!


Required keywords
^^^^^^^^^^^^^^^^^

.. container:: sphinx-custom

   ``singlet states: [integer]``

   Specifies the number of singlet excited states to calculate.

.. container:: sphinx-custom

   ``algorithm: [string]``

   Default: ``davidson`` for CCS, CC2, MLCC2, and CCSD; ``diis`` for lowmem-CC2 and CC3.

	Solver to use for converging the excited state equations. 

   Valid keyword values are:

   - ``davidson`` Use Davidson algorithm with residuals preconditioned with the orbital differences approximation of the Jacobian. Cannot be used for lowmem-CC2 and CC3.
   - ``diis`` Use DIIS algorithm with update estimates obtained from the orbital differences approximation of the
     Jacobian. Can be used for lowmem-CC2 and CC3.
   - ``non-linear davidson`` Use the non-linear Davidson algorithm with residuals preconditioned with the orbital
     differences approximation of the Jacobian. Can be used for lowmem-CC2 and CC3.
   - ``asymmetric lanczos`` Use the asymmetric Lanczos algorithm for excitation energies. EOM oscillator strengths will be calculated as well. Cannot be used for lowmem-CC2, MLCC2, and CC3.

.. container:: sphinx-custom

   ``chain length: [integer]``

   Specifies the dimension of the reduced (Krylov sub-) space for the asymmetric Lanczos algorithm. Required for ``algorithm: asymmetric lanczos``.

Optional keywords
^^^^^^^^^^^^^^^^^

.. container:: sphinx-custom

   ``right eigenvectors``

   Default: true

   If specified, solve for the right eigenvectors of the Jacobian matrix. This keyword should only be specified for an excited state calculation. For property calculations, the program will solve for both left and right vectors.  Optional.

   .. note::
      For response calculations or when using the asymmetric lanczos algorithm, this keyword is ignored.


.. container:: sphinx-custom

   ``left eigenvectors``

   Default: false

   If specified, solve for the left eigenvectors of the Jacobian matrix. This keyword should only be specified for an excited state calculation. For property calculations, the program will solve for both left and right vectors.  Optional.

   .. note::
      For response calculations or when using the asymmetric lanczos algorithm, this keyword is ignored.

.. container:: sphinx-custom

   ``energy threshold: [real]``

   Default: :math:`10^{-3}` (or 1.0d-3)

   Energy convergence threshold, as measured with respect to the previous iteration. Optional.
   
   .. note::
      The solvers will not check for the energy convergence, if the energy threshold is not set.

.. container:: sphinx-custom

   ``residual threshold: [real]``

   Default: :math:`10^{-3}` (or 1.0d-3)

   Threshold of the :math:`L^{2}`-norm of the residual vector of the excited state equation. Optional

.. container:: sphinx-custom

   ``core excitation: { integer, integer, ... }``

   Default: false

   Solve for core excitations within the CVS approximation. The integers specify which orbitals to excite out of. Orbitals are ordered according to orbital energy (canonical orbitals). If the keyword has been specified, CVS will be activated automatically. Optional.

.. container:: sphinx-custom

   ``remove core: { integer, integer, ... }``

   Default: false

   Valence excitations, but with excitations from core MOs projected out. The integers specify which core orbitals from which one should not excite. Orbitals are ordered according to orbital energy (canonical orbitals). Optional.

   .. note::
      This is the orthogonal projection to ``core excitation: { integer, integer, ... }``

   .. warning::
      Cannot be used in CVS calculations or when frozen core is enabled.

.. container:: sphinx-custom
   
   ``ionization``

   Default: false

   Solve for ionized state. If this keyword is specified, the ionized state will be calculated using a bath orbital and projection similar to CVS. Optional.

   .. note::
      For ionizations a bath orbital has to be requested in the :ref:`cc section <cc-section>`


.. container:: sphinx-custom

   ``max iterations: [integer]``

   Default: 100

   The maximum number of iterations. The solver stops if the number of iterations exceeds this number. 
   Optional.

.. container:: sphinx-custom

   ``diis dimension: [integer]``

   Default: 20

   Number of previous DIIS records to keep.  Optional.

   .. note::
      Only relevant for ``algorithm: diis``.

.. container:: sphinx-custom

   ``restart``

   Default: false

   If specified, the solver will attempt to restart from a previous calculation. Optional.

.. container:: sphinx-custom

   ``storage: [string]``

   Default: ``disk``

   Selects storage of excited state records. Optional. 

   Valid keyword values are:

   - ``memory`` Stores DIIS records in memory.
   - ``disk`` Stores DIIS records on file.

.. container:: sphinx-custom

   ``crop``

   Default: false

   If specified, the CROP version of DIIS will be enabled. Optional.

   .. note::
      Only relevant for ``algorithm: diis``.

.. container:: sphinx-custom

   ``max reduced dimension: [integer]``

   Default: 100

   The maximal dimension of the reduced space of the Davidson procedure. Optional.

   .. note::
      Only relevant for ``algorithm: davidson``.

.. container:: sphinx-custom

   ``lanczos normalization: [integer]``

   Default: ``asymmetric``

   Specifies the type of biorthonormalization for the asymmetric Lanczos algorithm. Optional.

   Valid keyword values are:

   - ``asymmetric`` Which enables :math:`\tilde{p} = p` and :math:`\tilde{q} = \frac{q}{p\cdot q}`
   - ``symmetric`` Which enables :math:`\tilde{p} = \frac{p}{\sqrt{|p\cdot q|}}` and :math:`\tilde{q} = \text{sgn}(p\cdot q)\frac{q}{\sqrt{|p\cdot q|}}`


.. container:: sphinx-custom

   ``max micro iterations: [integer]``

   Default: :math:`100`

   Maximum number of iterations in the non-linear Davidson solver. Optional.

.. container:: sphinx-custom

   ``davidson preconvergence``

   Performs pre-iterations with non-linear Davidson in the DIIS solver.

.. container:: sphinx-custom

   ``preconvergence threshold: [real]``

   Default: :math:`10^{-2}` (or 1.0d-2)

   Threshold for pre-iterations with non-linear Davidson in the DIIS solver.

.. container:: sphinx-custom

   ``max micro iterations: [integer]``

   Default: :math:`100`

   Maximum number of micro iterations in the non-linear Davidson solver.

.. container:: sphinx-custom

   ``rel micro threshold: [real]``

   Default: :math:`10^{-1}` (or 1.0d-1)

   Threshold for convergence in the micro iterations of the non-linear Davidson solver. This threshold is relative to the current norm of the residuals.
