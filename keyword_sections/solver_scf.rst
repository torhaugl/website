
solver scf
----------

Keywords related to the SCF solver go into the ``solver scf`` section.
This section is optional. If it is not given, defaults will be used for all keywords.

Optional Keywords
^^^^^^^^^^^^^^^^^

.. container:: sphinx-custom

   ``algorithm: [string]``
   
   Default: ``scf-diis``
   
   Selects the solver to use.
   
   Valid keyword values are:
   
   
   - ``scf-diis`` AO-based self-consistent Roothan-Hall algorithm with direct inversion of the iterative subspace (DIIS) 	acceleration.  
   
   - ``scf`` Self-consistent Roothan-Hall algorithm.
   
   - ``mo-scf-diis``  MO-based self-consistent Roothan-Hall algorithm with DIIS acceleration. Recommended for multilevel Hartree-Fock (MLHF).

.. container:: sphinx-custom

   ``energy threshold: [real]``
   
   Default:
   :math:`10^{-7}` (or 1.0d-7)
   
   Energy convergence threshold, as measured with respect to the previous iteration. 
   
   .. note::
      The solvers will not check for the energy convergence, if the energy threshold is not set.

.. container:: sphinx-custom

   ``gradient threshold: [real]``
   
   Default:
   :math:`10^{-7}` (or 1.0d-7)
   
   Gradient threshold :math:`\tau`. 
   The equations have converged if :math:`\max \mathbf{G} < \tau`.
   

.. container:: sphinx-custom

   ``storage: [string]``
   
   Default: ``memory``
   
   Selects storage of DIIS records. 
   
   Valid keyword values are:
   
   - ``memory`` Stores DIIS records in memory.
   
   - ``disk`` Stores DIIS records on file.

.. container:: sphinx-custom

   ``print orbitals``
   
   Default: ``false``
   
   If specified, the \*mo_coefficients.out file is created and copied to the working directory by ``eT_launch``.


.. container:: sphinx-custom
   
   ``crop``
   
   Default: ``false``
   
   If specified, the conjugate residual with optimal trial vectors (CROP) version of DIIS will be enabled. 

.. container:: sphinx-custom

   ``cumulative fock threshold: [real]``
   
   Default:
   :math:`1.0` (or *1.0d0*)
   
   When the gradient max-norm reaches this threshold, the Fock matrix is built using the difference in the density matrix 	relative to the previous iteration. When the max-norm exceeds the threshold, the Fock matrix is built directly using the current density matrix. 


.. container:: sphinx-custom

   ``max iterations: [integer]``
   
   Default:
   :math:`100`
   
   The maximum number of iterations. The solver stops if the number of iterations exceeds this number. 

.. container:: sphinx-custom

   ``diis dimension: [integer]``
   
   Default: 
   :math:`8`
   
   Number of previous DIIS records to keep. 

.. container:: sphinx-custom

   ``restart``
   
   Default: ``false``
   
   If specified, the solver will attempt to restart from a previous calculation.

.. container:: sphinx-custom

   ``skip``
   
   Default: ``false``
   
   If specified, orbitals will be read from file, the convergence of 
   the gradient will be checked, but the rest of the SCF solver will be skipped.
   
   .. note::
      This is used to restart from eT v1.0.x, as the orbitals will not be flipped.

.. container:: sphinx-custom

   ``ao density guess: [string]``
   
   Default: ``sad``
   
   Which atomic orbital density matrix to use as start guess. 
   
   Valid keyword values are:
   
   - ``sad`` Use the superposition of atomic densities (SAD) guess. This is built on-the-fly by performing spherically averaged 	UHF calculations on each unique atom (and basis) in the specified molecular system.
   - ``core`` Use the density obtained by approximating the Fock matrix by its one-electron contribution and performing one Fock diagonalization.

.. container:: sphinx-custom

   ``coulomb threshold: [real]``
   
   Default: 
   :math:`10^{-6}*(\text{gradient threshold})`
   
   The threshold for neglecting Coulomb contributions to the two-electron part of the AO Fock matrix. 

.. container:: sphinx-custom

   ``exchange threshold: [real]``
   
   Default: 
   :math:`10^{-4}*(\text{gradient threshold})`
   
   The threshold for neglecting exchange contributions to the two-electron part of the AO Fock matrix. Must be higher than ``coulomb threshold``. If this is specified with a lower value than ``coulomb threshold``, it will be set equal to ``coulomb threshold``. See the output. 

.. container:: sphinx-custom

   ``integral precision: [real]``
   
   Default: 
   :math:`\text{(coulomb threshold)}^2`
   
   The :math:`\epsilon` value for Libint 2. Gives the precision in the electron repulsion integrals. 

   .. note::

      Changes dynamically during Fock construction to give the required precision in the Fock matrix and not the integrals (small density contributions require less accuracy in the integrals). 

   .. warning::

      The value does not guarantee the given precision. It is highly recommended to let the program handle the integral precision value. 

.. container:: sphinx-custom

   ``integral cutoff: [real]``
   
   Default: 
   :math:`\text{(coulomb threshold)}`

   Shell-pairs for which all the electron repulsion integrals are smaller than this value are neglected in the Fock matrix construction. 
