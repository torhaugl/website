visualization
-------------

The ``visualization`` section is used for plotting orbitals and densities. 

Keywords
^^^^^^^^

.. container:: sphinx-custom

   ``grid spacing: [real]``

   Default: :math:`0.1` (or 1.0d-1)

   Sets the spacing between grid points for the visualization given in Angstrom units. Optional.

.. container:: sphinx-custom

   ``grid buffer: [real]``

   Default: :math:`2.0` (or 2.0d0)

   Sets the distance between the edge of the grid (x, y, and z direction) and the molecule in Angstrom units. Optional.

.. container:: sphinx-custom

   ``plot hf orbitals: {[integer], [integer], ...}``

   Plots the canonical orbitals given in the comma separated list. Optional.

.. container:: sphinx-custom

   ``plot hf density``

   Plots the HF density. Optional.

.. container:: sphinx-custom

   ``plot hf active density``

   Plots the active HF density in the case of a reduced space calculation. Optional.

   .. note:: 
      This keyword is only read if ``hf`` is specified in the ``frozen orbitals`` section.

.. container:: sphinx-custom

   ``plot cc density``

   Plots the coupled cluster density. Optional. 

   .. note:: 
      This keyword is only read if ``cc mean value`` or ``response`` is specified in the ``do`` section.

.. container:: sphinx-custom

   ``plot transition densities``

   Plots the coupled cluster transition densities. Optional. 

   .. note:: 
      This keyword is only read if ``cc mean value`` or ``response`` is specified in the ``do`` section.

.. container:: sphinx-custom
                                 
   ``states to plot: {[integer], [integer], ...}``
   
   List of integers specifying which transition densities should be plotted.
   
   .. note::
      By default the densities of all requested states are plotted.
