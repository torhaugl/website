method
------

In the method section, the wavefunction method is specified. In the case of a post-HF calculation, both the reference wavefunction method and the post-HF method must be specified.

Hartree-Fock methods
^^^^^^^^^^^^^^^^^^^^

At the Hartree-Fock level of theory the following keywords may be specified:

- ``hf`` for restricted Hartree-Fock (RHF)
- ``mlhf`` for multilevel Hartree-Fock
- ``uhf`` for unrestricted Hartree-Fock

In coupled cluster calculations, either RHF or MLHF must be specified, in addition to the coupled cluster method.

Coupled cluster methods
^^^^^^^^^^^^^^^^^^^^^^^

The available coupled cluster methods are:

- ``ccs``
- ``cc2``
- ``lowmem-cc2``
- ``ccsd``
- ``ccsd(t)``
- ``cc3``
- ``mlcc2``
- ``mlccsd``

Other methods
^^^^^^^^^^^^^

- ``mp2``

Examples
^^^^^^^^

For a Hartree-Fock calculation, specify the type of wavefunction (``hf``, ``mlhf``, or ``uhf``) in the method section.

.. code-block:: none
	
	method
	   hf
	end method

To perform a coupled cluster calculation, both the coupled cluster method and the type of reference wavefunction must be specified.

.. code-block:: none
	
	method
	   hf
	   ccsd
	end method
