solver cc gs
------------

Keywords related to solving the ground state coupled cluster equations go into the ``solver cc gs`` section. This section is optional. If it is not given, defaults will be used for all keywords.

Keywords
^^^^^^^^

.. container:: sphinx-custom

   ``algorithm: [string]``

   Default: ``diis``

   Solver to use for converging the amplitude equations. Optional.

   Valid keyword values are:

   - ``diis`` Quasi-Newton algorithm accelerated by DIIS. Uses the orbital differences approximation of the coupled cluster Jacobian.
   - ``newton-raphson``. Newton algorithm accelerated by DIIS.

.. container:: sphinx-custom

   ``energy threshold: [real]``

   Default:
   :math:`10^{-5}` (or 1.0d-5)

   Energy convergence threshold, as measured with respect to the previous iteration. Optional.
   
   .. note::
      The solvers will not check for the energy convergence, if the energy threshold is not set.

.. container:: sphinx-custom

   ``omega threshold: [real]``

   Default:
   :math:`10^{-5}` (or 1.0d-5)

   Threshold of the :math:`L^{2}`-norm of the amplitude equations vector 
   :math:`\Omega_\mu = \langle \mu \vert \bar{H} \vert \text{HF} \rangle`.
   Optional.

.. container:: sphinx-custom

   ``crop``

   Default: false

   If specified, the CROP version of DIIS will be enabled. Optional.


.. container:: sphinx-custom

   ``max iterations: [integer]``

   Default: 100

   The maximum number of iterations. The solver stops if the number of iterations exceeds this number. 
   Optional.

.. container:: sphinx-custom

   ``max micro iterations: [integer]``
   
    Default: 100

   The maximum number of micro-iterations in the exact Newton solver (see ``algorithm: newton-raphson``). 
   The solver stops if the number of iterations exceeds this number.
   Optional.

.. container:: sphinx-custom

   ``rel micro iterations: [real]``

   Default: :math:`10^{-2}` (or 1.0d-2)

   The relative threshold :math:`\tau` used for micro-iterations by the exact Newton solver (see ``algorithm: newton-raphson``). The micro-iterations are considered converged if the :math:`L^{2}`-norm of the Newton equation is less than :math:`\tau \vert\vert \boldsymbol{\Omega} \vert\vert`. Optional.

.. container:: sphinx-custom

   ``storage: [string]``

   Default: ``disk``

   Selects storage of DIIS records in coupled cluster calculations. Optional. 

   Valid keyword values are:

   - ``memory`` Stores DIIS records in memory.
   - ``disk`` Stores DIIS records on file.

.. container:: sphinx-custom

   ``micro iteration storage: [string]``
   
   Default: value of the keyword `storage`

   Selects storage of records in the micro iterations (if any) in ground state coupled cluster calculations. Optional. 

   Valid keyword values are:

   - ``memory`` Stores records in memory.
   - ``disk`` Stores records on file.

.. container:: sphinx-custom

   ``diis dimension: [integer]``

   Default: 8

   Number of previous DIIS records to keep.  Optional.

.. container:: sphinx-custom

   ``restart``

   Default: false

   If specified, the solver will attempt to restart from a previous calculation. Optional.
