.. _basis-sets:

List of basis sets
==================

This is a list of the basis sets included in eT by default.
The basis set files are located in ``eT/ao_basis``.

STO-nG
-------

``STO-2G``, ``STO-3G``, ``STO-4G``, ``STO-5G``, ``STO-6G``

Pople
-----

``3-21G``, ``6-311++G(2d,2p)``, ``6-311+G(2d,p)``, ``6-311++G(3df,3pd)``, ``6-311G(d,p)``, ``6-311++G``, ``6-311++G*``, ``6-311++G**``, ``6-311+G``, ``6-311+G*``, ``6-311+G**``, ``6-311G``, ``6-311G*``, ``6-311G**``, ``6-31G(2df,p)``, ``6-31G(3df,3pd)``, ``6-31G(d,p)``, ``6-31++G``, ``6-31++G*``, ``6-31++G**``, ``6-31+G``, ``6-31+G*``, ``6-31+G**``, ``6-31G``, ``6-31G*``, ``6-31G**``

Dunning
--------
``aug-cc-pCV5Z``, ``aug-cc-pCVDZ``, ``aug-cc-pCVQZ``, ``aug-cc-pCVTZ``, ``aug-cc-pV5Z``, ``aug-cc-pV6Z``, ``aug-cc-pVDZ``, ``aug-cc-pVQZ``, ``aug-cc-pVTZ``, ``cc-pCV5Z``, ``cc-pCVDZ``, ``cc-pCVQZ``, ``cc-pCVTZ``, ``cc-pV5Z``, ``cc-pV6Z``, ``cc-pVDZ``, ``cc-pVQZ``, ``cc-pVTZ``, ``d-aug-cc-pV5Z``, ``d-aug-cc-pVDZ``, ``d-aug-cc-pVQZ``, ``d-aug-cc-pVTZ``

Ahlrichs
--------

   ``def2-QZVPD``, ``def2-QZVP``, ``def2-QZVPPD``, ``def2-QZVPP``, ``def2-SV``, ``def2-SVPD``, ``def2-SV(P)``, ``def2-SVP``, ``def2-TZVPD``, ``def2-TZVP``, ``def2-TZVPPD``, ``def2-TZVPP``

Other
-----

   ``ANO-RCC``, ``mini``


