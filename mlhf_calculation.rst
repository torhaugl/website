.. _mlhf-calc:

Multilevel Hartree-Fock calculation
======================================

Setting up an MLHF calculation
-------------------------------

To run an MLHF calculation, the method ``mlhf`` must be specified in the :ref:`method section <method-section>`.

.. code-block:: none

   method
      mlhf
   end method
   
Furthermore, you must request a ``ground state`` calculation in the :ref:`do section <do-section>`.

.. code-block:: none 

   do
     ground state
   end do
   
As MLHF calculations run in the MO basis, it is also necessary to specify 
the ``mo-scf-diis`` algorithm in the :ref:`solver scf keyword section <scf-section>`.

.. code-block:: none

   solver scf
      algorithm: mo-scf-diis
   end solver scf

Additionally, an active atom space has to be defined through the :ref:`active atoms section<active-atoms-section>`. 
In this section a ``selection type`` has to be chosen. The options are ``list``, ``range`` and ``central atom``.
For MLHF this keyword is followed by the keyword ``hf`` (for Hartree-Fock) where the active atoms are specified.
For the ``selection type`` ``list`` a list of numbers has to be specified where each number refers to an atom in the geometry.
For the ``selection type`` ``range`` 2 numbers has to be specified where each number refers to an atom in the geometry.
All atoms from the first to the second number are selected.
For the ``selection type: central atom`` one atom central atom is chosen by giving a number to the ``central atom`` keyword.
The active atoms are chosen depending on the range from this central atom. The range is given as floating point number in the ``hf`` keyword.

A minimal working example for an MLHF single-point calculation on 2 water molecules can be found below.
The first water molecule (atoms 1, 2 and 3) is selected as active while the other water molecule is inactive.

.. code-block:: none

   system
      name: 2 waters
   end system

    method
       mlhf
    end method
    
   do
      ground state
   end do
    
   solver scf
      algorithm: mo-scf-diis
   end solver scf

   active atoms
      selection type: range
      hf: [1,3]
   end active atoms

   geometry
   basis: aug-cc-pVDZ
   H          0.86681        0.60144        5.00000
   H         -0.86681        0.60144        5.00000
   O          0.00000       -0.07579        5.00000
   H          0.86681        0.60144        0.00000
   H         -0.86681        0.60144        0.00000
   O          0.00000       -0.07579        0.00000
   end geometry
   
Save this as ``2_waters.inp`` and type the following command in your terminal (here shown for four threads)

.. code-block:: none

   eT_launch --omp 4


After the calculation finished you should find ``2_waters.out`` and ``2_waters.timing.out`` in your working directory. 
Obtaining the timing file can be suppressed by specifying the ``-nt`` flag after 
`eT_launch <https://gitlab.com/eT-program/eT/-/wikis/Using-eT/How-to-use-the-launch-script>`_.
If the calculation exited successfully (look for ``eT terminated successfully!`` at the bottom of the file), 
the output file should show iteration information for the solver converging the MLHF equations. 
The table with the iteration information should end with something like this:

.. code-block:: none

   ...
     7          -151.950133335899     0.1229E-04     0.3615E-08
     8          -151.950133335979     0.1492E-05     0.8060E-10
     9          -151.950133335983     0.3509E-06     0.3524E-11
   ---------------------------------------------------------------
   Convergence criterion met in 9 iterations!


An optional :ref:`multilevel hf section <mlhf-section>` can be specified in an MLHF calculation 
to request specific settings such as generating Cholesky active virtual orbitals, 
an initial full space HF optimization to a given threshold, or a projection on a minimal basis. 
In this last case, the section would look like the following example:

.. code-block:: none

   multilevel hf
      project on minimal basis
   end multilevel hf

Relevant input sections
-----------------------

:ref:`Active atoms <active-atoms-section>`

:ref:`Multilevel Hartree-Fock <mlhf-section>`

:ref:`Solver SCF <scf-section>`
