.. _mlcc-calc:

Multilevel coupled cluster calculation
======================================

Currently, only ground and excited state multilevel CC2 (MLCC2) calculations can be performed with eT.

Setting up an MLCC2 or MLCCSD calculation
-----------------------------------------

To run an MLCC2 or MLCCSD calculation, the method ``mlcc2``/``mlccsd`` must be specified in the :ref:`method section <method-section>`.
E.g. for MLCC2

.. code-block:: none

   method
      hf
      mlcc2
   end method

As multilevel methods are especially suited for intensive properties, 
its most relevant usage is for excited state calculations:

.. code-block:: none

    do
       excited state
    end do

The :ref:`mlcc section <mlcc-section>` is necessary for every MLCC calculation, 
it is used to specify how the active space is generated. 
E.g., for MLCC2 with approximated correlated natural transition orbitals (CNTOs) 
tailored for the lowest excited state:

.. code-block:: none
	
    mlcc
       levels: ccs, cc2
       cc2 orbitals: cnto-approx
       cnto states: {1}
    end mlcc

For MLCCSD (CCS/CCSD) with approximated CNTOs tailored for the lowest excited state:

.. code-block:: none
	
    mlcc
       levels: ccs, ccsd
       ccsd orbitals: cnto-approx
       cnto states: {1}
    end mlcc

If Cholesky or Cholesky-PAO orbitals are used, an active atom space must also be defined 
through the :ref:`active atoms section<active-atoms-section>`. 
In the example below for two waters, the first water is selected as active (atoms 1, 2, and 3)

.. code-block:: none

    system
       name: 2 H2O
    end system

    method
       hf
       mlcc2
    end method

    do
       excited state
    end do

    solver cc es
       singlet states: 2
    end solver cc es

    mlcc
       levels: ccs, cc2
       cc2 orbitals: cholesky-pao
    end mlcc

    active atoms
       selection type: range
       cc2: [1,3]
    end active atoms

    geometry
    basis: aug-cc-pVDZ
    H          0.86681        0.60144        5.00000
    H         -0.86681        0.60144        5.00000
    O          0.00000       -0.07579        5.00000
    H          0.86681        0.60144        0.00000
    H         -0.86681        0.60144        0.00000
    O          0.00000       -0.07579        0.00000
    end geometry

Save this as ``mlcc2.inp`` and type the following command in your terminal (here shown for four threads)

.. code-block:: none

   eT_launch --omp 4

After the calculation finished you should find ``mlcc2.out`` and ``mlcc2.timing.out`` in your working directory. 
Obtaining the timing file can be suppressed by specifying the ``-nt`` flag after 
`eT_launch <https://gitlab.com/eT-program/eT/-/wikis/Using-eT/How-to-use-the-launch-script>`_.
If the calculation exited successfully (look for ``eT terminated successfully!`` at the bottom of the file), 
a summary of the excited state calculation should be printed at the end of the ``mlcc2.out`` file

.. code-block:: none

   ...
   - Electronic excitation energies:

                                      Excitation energy
                          ------------------------------------------
      State                (Hartree)             (eV)
     ---------------------------------------------------------------
        1                  0.222134776911        6.044595162993
        2                  0.275347045324        7.492574739770
     ---------------------------------------------------------------
     eV/Hartree (CODATA 2014): 27.21138602

   - Finished solving the MLCC2 excited state equations (right)
   ...

Relevant input sections
-----------------------

:ref:`Active atoms <active-atoms-section>`

:ref:`Coupled cluster excited state <cc-es-section>`

:ref:`Coupled cluster ground state <cc-gs-section>`

:ref:`Multilevel coupled cluster <mlcc-section>`


