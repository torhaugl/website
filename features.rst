.. _features-eT:

Features
==========

The eT program is first and foremost a coupled cluster program, 
with the CCS, CC2, CCSD, CCSD(T), and CC3 methods implemented. 
In addition to the standard coupled cluster methods, 
eT has multilevel CC2, multilevel CCSD, 
and coupled cluster time propagation. 
Hartree-Fock calculations, 
both restricted closed shell and unrestricted, 
are available. 
Furthermore, the multilevel Hartree-Fock method is implemented. 
QM/MM is available at both HF and CC level.

A detailed description of the features available for each method is given below.


Hartree-Fock
------------

The available Hartree-Fock methods are

- Restricted Hartree-Fock (RHF)
- Unrestricted Hartree-Fock (UHF)
- Multilevel Hartree-Fock (MLHF)

Restricted and unrestricted Hartree-Fock
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Restricted Hartree-Fock (RHF) can be used for single-point calculations, 
geometry optimizations, 
properties (dipole and quadrupole), 
and as a reference wavefunction for coupled cluster calculations. 

Unrestricted Hartree-Fock (UHF) can be used for single-point calculations.

:ref:`Setting up a Hartree-Fock calculation <hf-calc>`


Multilevel Hartree-Fock
^^^^^^^^^^^^^^^^^^^^^^^

Multilevel Hartree-Fock can be used for single-point calculations, 
and as a reference wavefunction for reduced space coupled cluster calculations.

:ref:`Setting up a multilevel Hartree-Fock calculation <mlhf-calc>`

Coupled cluster
---------------

The implemented coupled cluster methods in eT are

- Standard methods: CCS and CCSD
- Perturbative methods: CC2, CC3 and CCSD(T)
- Multilevel CC2 and CCSD

Additionally, the code can perform time-propagation of some of the implemented coupled cluster wavefunctions.

The features implemented for the coupled cluster methods vary somewhat, and are detailed below.

:ref:`Setting up a coupled cluster calculation <cc-calc>`

CCS, CC2, CCSD and CC3
^^^^^^^^^^^^^^^^^^^^^^

For CCS, CC2, CCSD, and CC3, eT offers ground and excited state calculations in addition to dipole and quadrupole moments, and EOM oscillator strengths.

For CCS, and CCSD also EOM polarizabilities are implemented.

CCSD(T)
^^^^^^^
Ground state energies at the CCSD(T) level of theory are implemented.

Low-memory CC2
^^^^^^^^^^^^^^
In eT, there are two versions of the CC2 code. The standard CC2 code has a memory requirement proportional to 
:math:`n_o^2 n_v^2`
, where 
:math:`n_o` 
is the number of occupied orbitals and
:math:`n_v`
is the number of virtual orbitals.
However, the low-memory CC2 implementation has an 
:math:`N^2`
memory requirement, where 
:math:`N`
is the number of orbitals.

Currently, only ground and excited state energies are available with the low-memory CC2 code.

Multilevel Coupled Cluster
^^^^^^^^^^^^^^^^^^^^^^^^^^
The multilevel CC2 (MLCC2) and multilevel CCSD (MLCCSD) methods 
are available with correlated natural transition orbitals, 
Cholesky orbitals (occupied and virtual), and projected atomic orbitals.

Currently, only ground and excited states are available at the MLCCSD level of theory.

:ref:`Setting up a multilevel coupled cluster calculation <mlcc-calc>`

Time-dependent coupled cluster
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Real-time propagation is available for CCS and CCSD methods. 
It solves the differential equations describing the time evolution 
of cluster amplitudes and Lagrange multipliers. 
The available integrators are

- Euler
- Gauss-Legendre (with order 2, 4 and 6)
- Runge-Kutta (with order 4)

The time dependent quantities that are available as output are:

- Amplitudes
- Multipliers
- Density matrix
- Energy
- Electric field
- Dipole moment

In addition, visualizable time-dependent density and spectra given by the Fast Fourier Transform 
of the dipole moment and of the electric field can be requested as output.

Environments
^^^^^^^^^^^^
The available QM/MM methods are

- Electrostatic embedding (non-polarizable)
- Polarizable QM/Fluctuating charges
- Polarizable Continuum Model

Both the methods can be coupled both with HF or CC wavefunctions. 
In case of QM/FQ, only HF ground state Fock and MOs are affected by QM/FQ.

Cholesky decomposition of the ERIs
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
The coupled cluster code is based on Cholesky decomposed electron repulsion integrals (ERIs). 
For sufficiently small systems, 
T1-transformed ERIs are constructed from the Cholesky vectors and stored in memory. 
For systems where the ERIs cannot be placed in memory, 
the T1-transformed Cholesky vectors are stored in memory if possible 
and ERIs are constructed from these vectors on the fly. 
For larger systems, 
the Cholesky vectors are stored on disk.

Visualization
^^^^^^^^^^^^^
Ground state densities can be written to .plt files that are readable by Chimera 
for both Hartree-Fock and coupled cluster. 
For coupled cluster, 
it is also possible to plot transition densities. 
