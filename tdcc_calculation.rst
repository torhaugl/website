Time-dependent coupled cluster calculation
===========================================
Explicitly time-dependent coupled cluster calculations are currently available for CCS and CCSD methods. A general description of how to perform generic coupled cluster calculations is given in the ``Coupled cluster calculation`` tutorial. In addition to the sections that are needed for a ground state coupled cluster calculation with multipliers, the ``time dependent state`` keyword must be specified in the ``do`` section:

.. code-block:: none

   do
     time dependent state
   end do

Propagation
-----------

Real-time propagation will be done if it is specified in the ``td cc`` section:

.. code-block:: none

   cc td
      propagation
   end cc td

The details of the real-time propagation must be given in the ``solver cc propagation`` section. In this section, the keywords ``initial time``, ``final time`` and ``time step`` must be specified together with corresponding arguments in Hartree atomic units, as in the following example:

.. code-block:: none

   solver cc propagation
      initial time: 0.0
      final time: 10.0
      time step: 0.01
   end solver cc propagation

This is all the additional information you are required to specify in order to run a real-time propagation with the default settings. A minimal working example is the following:

.. code-block:: none

   system
      name: H2OHe
   end system

   do
      time dependent state
   end do

   method
      hf
      ccsd
   end method

   cc td
      propagation
   end cc td

   solver cc propagation
      initial time: 0.0
      final time: 10.0
      time step: 0.05
   end solver cc propagation

   geometry
   basis: cc-pVDZ
      H          0.86681        0.60144        5.00000
      H         -0.86681        0.60144        5.00000
      O          0.00000       -0.07579        5.00000
      He         0.10000       -0.02000        7.53000
   end geometry

It is possible to produce output files containing the time series for the energy, dipole moment, density matrix, cluster amplitudes and Lagrange multipliers of the calculation, by adding the corresponding keywords, namely ``energy output``, ``dipole moment output``, ``density matrix output``, ``amplitudes output`` and ``multipliers output`` in the ``solver cc propagation`` section. For example, if an output file containing the energy time series is desired, one can write the ``solver cc propagation`` section as:

.. code-block:: none

   solver cc propagation
      initial time: 0.0
      final time: 10.0
      time step: 0.05
      energy output
   end solver cc propagation

In the output files, the first column contains the time in Hartree atomic units, while the other columns contain the time-dependent value(s) of the required quantity, in Hartree atomic units or without units.
For example, the second and following columns of a dipole moment file are, in order: real part of dipole moment along x-axis, imaginary part of dipole moment along x-axis, real part of dipole moment along y-axis, imaginary part of dipole moment along y-axis, real part of dipole moment along z axis and imaginary part of dipole moment along z axis.

The default is that the values of the quantities requested for output are written to file at each time step. However, it is possible to decide how many time steps should pass between each time they are written to file by giving the desired number of passes as an argument to the keyword ``steps between output``, as in the following example:

.. code-block:: none

   solver cc propagation
      initial time: 0.0
      final time: 10.0
      time step: 0.05
      steps between output: 20
      energy output
   end solver cc propagation

Another parameter that can be set is the convergence threshold for the solution of equations in implicit integration methods. The default is 1.0e-11, but this value can be given as the argument of the keyword ``implicit threshold``, as in the following example:

.. code-block:: none

   solver cc propagation
      initial time: 0.0
      final time: 10.0
      time step: 0.05
      implicit threshold: 1.0d-10
      energy output
   end solver cc propagation

Finally, the integration can be done using the Euler method, the most well-known four-step fourth-order explicit Runge-Kutta method known as RK4 (default), and the 2nd, 4th and 6th order Gauss-Legendre methods. The integration method is chosen by writing respectively ``euler`` , ``rk4`` , ``gl2`` , ``gl4`` or ``gl6`` as the argument of the keyword ``integrator`` in the ``solver cc propagation`` section. This is illustrated in in the following example:

.. code-block:: none

   solver cc propagation
      initial time: 0.0
      final time: 10.0
      time step: 0.05
      integrator: gl4
      energy output
   end solver cc propagation

External electric field
-----------------------
It is possible for the user to specify any number of classical electric field pulses within the dipole approximation that should interact with the molecule during the real-time propagation. Each pulse is defined by a set of parameters, which must be specified through keywords in the ``electric field`` section as follows:

.. code-block:: none

   electric field
      keyword: {argument for pulse 1, argument for pulse 2, ...}
   end electric field

The parameter keywords that can be specified in the electric field section are the following:

   ``envelope``       :  1 for Gaussian envelope, 2 for sine squared envelope

   ``x polarization`` :  x component of the unit polarization vector

   ``y polarization`` :  y component of the unit polarization vector

   ``z polarization`` :  z component of the unit polarization vector

   ``central time``   :  central time of both the Gaussian and sine squared envelopes, in Hartree atomic units

   ``width``          :  Gaussian RMS width/standard deviation of Gaussian envelopes, single period (truncated outside this) for sine squared envelopes, in Hartree atomic units

   ``central angfreq``:  central angular frequency of the carrier wave, in Hartree atomic units

   ``peak strength``  :  peak electric field strength of the pulse, in Hartree atomic units

   ``phase shift``    :  (carrier-envelope) phase shift, given as the shift between the maxima of the carrier wave and the central time of the envelope

   ``repetition``     :  number of repetitions of given pulse (default is 1)

   ``separation``     :  temporal separation between each of the repeated pulses (needed only when the number of repetitions is greater than one)

When the interaction with a classical electric field is requested, all of the above parameters apart from ``repetition`` and ``separation`` must be specified. The corresponding arrays of arguments also must have the same length, as in the following example:

.. code-block:: none

   electric field
       envelope: {1, 1}
       x polarization: {0.0, 0.0}
       y polarization: {0.0, 0.0}
       z polarization: {1.0, 1.0}
       central time: {100.0, 200.0}
       width: {10.0, 20.0}
       central angular frequency: {0.1, 0.2}
       peak strength: {0.01, 0.02}
       phase shift: {0.0, 0.0}
   end electric field

Fast Fourier Transform
----------------------

In a time-dependent calculation, it is possible to request the complex Fast Fourier Transform (FFT) of the time-dependent dipole moment and electric field in a specified interval.
For example, to compute the FFT of the dipole moment, the section ``solver fft dipole moment`` should be requested. 

The keywords that must be specified with corresponding arguments in the ``solver fft dipole moment`` section are the following:

   ``initial time``: the first temporal value to be considered in the FFT

   ``final time``  : the first last temporal value to be considered in the FFT

   ``time step``   : should equal the product between the value of ``time step`` and ``steps between output``, both from the ``solver cc propagation`` section

The same holds for the fast Fourier transform of the electric field, with the only difference that in this case the section name should be changed to ``solver fft electric field``.

Relevant input sections
-----------------------

:ref:`Time-dependent CC section<cc-td-section>`

:ref:`Propagation section<propagation-section>`

:ref:`Electric field section<electric-field-section>`

:ref:`Solver FFT section<solver-fft-section>`
