.. _user-manual:

User manual
=============

   See the `readme <https://gitlab.com/eT-program/eT/blob/master/README.md>`_ 
   for information on how to obtain and install eT.

.. toctree::
   :maxdepth: 1

   setup.rst
   eT_launch.rst

Building an eT input file
----------------------------------

.. toctree::
   :maxdepth: 1

   anatomy.rst
   required_sections.rst
   list_of_all_keywords.rst
   basis.rst

Calculation tutorials
----------------------

.. toctree::
   :maxdepth: 1

   hf_calculation.rst
   mlhf_calculation.rst
   cc_calculation.rst
   mlcc_calculation.rst
   tdcc_calculation.rst
   restart_calculation.rst


